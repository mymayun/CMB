rm(list = ls())
data<-read.table("./sequences.txt",header = FALSE)

#init
N<-dim(data)[1]
L<-nchar(as.character(data[2,1]))
K<-6
M<-4
W=L-K+1#motif可能出现位置的最大值
X<-matrix(0,N,L)
rule<-data.frame(t(c(1,2,3,4)))
colnames(rule)<-c("A","C","G","T")
point<-rep(0,N)
for (i in 1:N){
  for (j in 1:L){
    Nucleo<-substr(as.character(data[i,]),j,j)
    X[i,j]<-rule[,Nucleo]
  }
}

#lamda表示每个component的权重，用来表示每个component对Y贡献多少
lamda<-runif(W,0.0,1.0)
lamda<-lamda/sum(lamda)#normalize

#theta0表示背景分布的每种可能性的概率大小
#随机初始化
theta0<-runif(M,0.0,1.0)
theta0<-theta0/sum(theta0)

#theta表示前景分布的每种可能性的概率大小
#每一行表示motif区域每一列的概率分布情况
theta<-matrix(1/M,K,M)

#用来记录Y的值
EY<-matrix(0,N,W)
EY_sum<-matrix(0,N,W)

lamda_ed<-lamda  
theta_ed<-theta
theta0_ed<-theta0

#threshold
iter_threshold <- 5e-5
iter_number <- 0

while (iter_number<100){
  error<-0
  #E-step	
  for (i in 1:N){
    #计算每个可能作为motif起始点的位点的值
    #如果起始位点在j
    for (j in 1:W){
      EY_sum[i,j]<-1
      #在确定其实位点为j的情况下，遍历整条序列，计算在不同j的情况下Y的值，并累加
      for (l in 1:L){
        #如果是motif部分的碱基则按照前景分布情况处理
        if (l>=j & l<(j+K)){
          EY_sum[i,j]<-EY_sum[i,j]*theta[l-j+1,X[i,l]]
        }
        #如果不是motif部分的碱基则按照背景分布情况处理
        else{
          EY_sum[i,j]<-EY_sum[i,j]*theta0[X[i,l]]
        }			
      }
      EY_sum[i,j]<-EY_sum[i,j]*lamda[j]
    }
    EY[i,]<-EY_sum[i,]/sum(EY_sum[i,])
  }
  
  #M-step
  #求解lamda的估计值
  for (j in 1:W ){  
    lamda[j]=sum(EY[,j])/N
  }
  lamda<-lamda/sum(lamda)
  
  #求解背景分布theta0的估计值
  #使用预测得到的参数更新背景分布参数theta0
  for (m in 1:M){
    theta0[m]<-0
    for (i in 1:N){
      for (l in 1:L){
        for (j in 1:W){
          #非motif区域特定碱基出现EY[i,j]的概率累加
          theta0[m]<-theta0[m]+EY[i,j]*(X[i,l]==m)*((l<j)+(l>j+K-1))
        }
      }
    }
  }
  if (sum(theta0)==0) next
  else{
    theta0<-theta0/sum(theta0)
  }
  

  #third part theta
  #使用预测得到的参数更新前景分布参数theta
  for (l in 1:K){	
    for (m in 1:M){
      theta[l,m]<-0
      for (i in 1:N){
        for (j in 1:W){
          theta[l,m]<-theta[l,m]+EY[i,j]*(X[i,l+j-1]==m) # K
        }
      }
    }
    theta[l,]<-theta[l,]/sum(theta[l,])
  }
  
  error<-sum(abs(lamda-lamda_ed))/W+sum(abs(theta0-theta0_ed))/M+sum(abs(theta-theta_ed))/(K*M)
  print(error)
  if (error<iter_threshold){
    iter_number<-iter_number+1
  }else{
    lamda_ed<-lamda  
    theta_ed<-theta
    theta0_ed<-theta0
  }
  
  for (i in 1:N){ 
    point[i]<-which.max(EY[i,])
  }
}
print(lamda)
print(theta0)
print(theta)
print(point)
print(error)

write.table(lamda,"./output/lamda.txt",sep = "\t",row.names = FALSE,col.names = FALSE, quote = FALSE,fileEncoding = "UTF-8")
write.table(theta0,"./output/theta0.txt",sep = "\n",row.names = FALSE,col.names = FALSE, quote = FALSE,fileEncoding = "UTF-8")
write.table(t(theta),"./output/theta.txt",sep = "\t",row.names = FALSE,col.names = FALSE, quote = FALSE,fileEncoding = "UTF-8")

motifs <- c()
for(i in 1:nrow(data)){
  motifs <- c(motifs,substr(data[i,1],point[i],point[i]+5))
}
motifs.count <- table(motifs)
motifs.count <- as.data.frame(motifs.count)
motifs.count <- motifs.count[order(-motifs.count$Freq),]

write.table(motifs.count,"./output/motifs.counts.txt",sep = "\t",row.names = FALSE,col.names = FALSE,quote = FALSE,fileEncoding = "UTF-8")












